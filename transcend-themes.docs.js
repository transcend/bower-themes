/**
 * @ngdoc overview
 * @name transcend.themes
 * @description
 # Themes Module
 The "Themes" module provides all....

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-themes.git
 ```

 Other options:

 *   * Download the code at [http://code.tsstools.com/bower-themes/get/master.zip](http://code.tsstools.com/bower-themes/get/master.zip)
 *   * View the repository at [http://code.tsstools.com/bower-themes](http://code.tsstools.com/bower-themes)
 *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/themes/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.themes']);
 ```

 ## Configuration
 You can configure the {@link transcend.themes Themes Module} by using the standard $provider configuration method
 or by overriding the "{@link transcend.themes.themesConfig themesConfig}" object. This object is passed into each component in the
 module and will use any properties set on the object before deferring to the default settings/configuration.

 ```
 angular.module('myApp').value('themesConfig', {
   resource: {
     // Override the default URL for all resources in this module.
     url: 'http://locahost/MyWebApiApp'
   }
 });
 ```

 ## Usage
 By default, the {@link transcend.themes Themes Module} is configured to work out of the box with an MVC5 WebAPI2
 back-end - with individual accounts setup (using Bearer token). For additional information on setting up the backend
 project to support this front-end control, see the "MVC5 and WebAPI Authentication" section of the
 {@link development/resources Resources Page}.

 ## To Do
 - Add additional components for registration, password change, and role management.

 ## Module Goals
 - Keep the minified module under 5KB.
 */
/**
 * @ngdoc object
 * @name transcend.themes.themesConfig
 *
 * @description
 * Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any
 * components in the {@link transcend.themes Themes Module}. The "themesConfig" object is a
 * {@link http://docs.angularjs.org/api/auto/object/$provide#value module value} and can therefore be
 * overridden/configured by:
 *
 <pre>
 // Override the value of the 'themes' property.
 angular.module('myApp').value('themesConfig', {
    // This would limit the list to only 2 available themes.
     themes: [
       'cartographic',
       'cerulean'
     ]
   });
 </pre>
 **/
/**
 * @ngdoc object
 * @name transcend.themes.themesConfig#themes
 * @propertyOf transcend.themes.themesConfig
 *
 * @description
 * List of available themes. The default value of this list is all avialable themes.
 *
 <pre>
 angular.module('myApp').value('themesConfig', {
       themes: [
        'cartographic',
        'cerulean',
        'contrast',
        'cosmo',
        'cyborg',
        'darkly',
        'flatly',
        'journal',
        'khaki',
        'lumen',
        'midnight',
        'mint',
        'ocean',
        'paper',
        'patriot',
        'purpley',
        'readable',
        'royal',
        'sandstone',
        'simplex',
        'slate',
        'spacelab',
        'sunrise',
        'superhero',
        'transcend',
        'united',
        'yeti'
       ]
     });
 </pre>
 *
 * __Note,__ There must be an associated "css" file to match this theme.
 */
/**
 * @ngdoc directive
 * @name transcend.themes.directive:themeMenu
 *
 * @description
 * The 'themeMenu' directive provides a simple dropdown menu component that will allow selecting (binding) to a theme.
 *
 * Note: This component will set $rootScope.theme to the selected theme value.
 *
 <pre>
 // Typical usage in navbar:
 <li theme-menu></li>
 </pre>
 *
 * @restrict EA
 * @element ANY
 * @scope
 *
 * @requires $rootScope
 * @requires transcend.core#appConfig
 * @requires transcend.themes#themesConfig
 */
